# local-impl

A proc macro for creating extension traits. This has been refactored out of
[spade-lang/spade](https://gitlab.com/spade-lang/spade/-/tree/master/parse_tree_macros).

See the documentation for usage examples.

## License

Licensed under the EUPL-1.2-or-later
